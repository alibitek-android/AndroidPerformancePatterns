# [Android Development Patterns](https://www.youtube.com/playlist?list=PLWz5rJ2EKKc-lJo_RGGXL2Psr8vVCTWjM)

- [Android Development Patterns - YouTube playlist](https://www.youtube.com/playlist?list=PLWz5rJ2EKKc-lJo_RGGXL2Psr8vVCTWjM)
- [Android Development with Ian Lake](https://www.youtube.com/playlist?list=PL7kiz-Bl7dzgBfEVJ7S9f9FkTF6OR1J_q)
- [Build better apps](https://medium.com/tag/buildbetterapps)
- https://github.com/mvescovo/moviehotness


# [Android Performance Patterns](https://www.youtube.com/playlist?list=PLWz5rJ2EKKc9CBxr3BVjPTPoDPLdPIFCE)

- "Everything in performance is about understanding the caveats and choosing the best options. And trust me, caveats are everywhere."
- "Keep calm. Profile your code and always remember - perf matters!"

Everyone discovers content in different ways and those that watch a video might not ever read the documentation or a blog post and vice versa (thanks for going to extra mile!). Keeping them in sync and properly aging out documentation is a big goal going forward.

- [Android Performance Patterns - YouTube playlist](https://www.youtube.com/playlist?list=PLWz5rJ2EKKc9CBxr3BVjPTPoDPLdPIFCE)
- [Android Performance Patterns - Google+ Community](https://plus.google.com/communities/116342551728637785407)
- [Android Developers - YouTube channel](https://www.youtube.com/channel/UCVHFbqXqoYvEWM1Ddxl0QDg?sub_confirmation=1)
- https://developer.android.com/samples/DisplayingBitmaps/src/com.example.android.displayingbitmaps/util/DiskLruCache.html
- https://developer.android.com/guide/components/loaders.html
- [AT&T ARO - Application Resource Optimizer](https://developer.att.com/application-resource-optimizer)
- [AT&T Network Attenuator Beta](https://developer.att.com/developer/legalAgreementPage.jsp?passedItemId=14500040)
- [Cellular Data Network Simulator](https://github.com/Polidea/Cellular-Data-Network-Simulator)
- [Network Speed Emulation](https://developer.android.com/tools/devices/emulator.html#netspeed)
- [GcmNetworkManager](https://developers.google.com/android/reference/com/google/android/gms/gcm/GcmNetworkManager)
- [Network traffic tool](https://developer.android.com/tools/debugging/ddms.html#network)
- [Transmitting Network Data Using Volley](https://developer.android.com/training/volley/index.html)
- [Network Connection Class](https://code.facebook.com/projects/1547113495553528/network-connection-class/)
- [OkHttp](https://square.github.io/okhttp/)
- [Picasso](https://square.github.io/picasso/)
- [ProGuard](https://developer.android.com/tools/help/proguard.html)
- [Google I/O 2015 app](https://github.com/google/iosched)
- [Building Apps with Over 65K Methods](https://developer.android.com/tools/building/multidex.html)
- [Systrace](https://developer.android.com/tools/help/systrace.html)
- [IntentService](https://developer.android.com/reference/android/app/IntentService.html)
- [Google Cloud Messaging](https://developers.google.com/cloud-messaging)
- [Broadcast Receiver](https://developer.android.com/reference/android/content/BroadcastReceiver.html)
## Season 1

## Season 2

## Season 3

## Season 4
- Episode 1
- Episode 2
- Episode 3
- Episode 4
- Episode 5
- Episode 6
- Episode 7
- [Episode 8 - Removing unused resources](https://www.youtube.com/watch?v=HxeW6DHEDQU&index=18&list=PLWz5rJ2EKKc9CBxr3BVjPTPoDPLdPIFCE)
    - Description:   
    `Ok ok, so you’ve watched all the Android Performance Patterns videos, and you know how important it is to be diligent when it comes to bloated APKs. In fact, as soon as a layout file, or bitmap isn’t needed any more by your app, you remove it from the correct folder right away. Good for you!
But.. then.. why is your APK still bloated?
As Colt McAnlis will unveil in this video, although YOU may be diligent in removing resources your app won’t use, it’s not safe to assume that the LIBRARIES you’re including will do the same. Thankfully some helpful Gradle tools can put your APK on a diet.`
    - Links:
    - Comments:
        - Need a good guide on creating ProGuard rules to stop it from removing files created by third-party libraries.﻿
        - True, whatever I enable minify it breaks something.﻿
- [Episode 14 - Serialization performance](https://www.youtube.com/watch?v=IwxIIUypnTE&list=PLWz5rJ2EKKc9CBxr3BVjPTPoDPLdPIFCE&index=24)
    - Description: `Data Serialization is an important aspect of every android application; but when it comes to performance, there's a right, and a wrong way to do it. For years now, developers have been leveraging JSON (and a few still use XML) for their needs. But in this video Colt McAnlis gives you the skinny on why these human-readable formats are bad for your app. In fact your choice of serialization provider and format, can have a huge impact on your app's' performance.`
    - Notes:
        - Serialization is the process of taking some in-memory object and converting it to a formatted chunk of data that can be converted back (deserialization) to an in-memory object some time later.
        - Typically most Android developers will default to the easiest method to serialize their data (e.g. Serializable, ObjectOutputStream), but this approaches have significant memory and encoding overhead, making them entirely too slow for the job at hand.
        - For the sake of performance you want to avoid all that chaos and instead look at things like:
            - [GSON](https://github.com/google/gson) library produces much faster serialization and more memory efficient results, although there's a large problem with GSON, in that, it uses the JSON format which is known to produce bloated files. (the format is flexibile in terms of data changes or order changes and spaces, quotes, returns, names, are all verbose data.)
            - Android resource file which are XML fornmatted don't have any of these problems, because they are compiled at build time into a more compact format that is more memory efficient and fast to load, so you don't need to worry about any of those.
            - Smaller is better. It's a binary decision. And lucky for you we have three great options available:
                1. [Protocol Buffers](https://developers.google.com/protocol-buffers/) 
                2. [Nano-Proto-Buffers](https://android.googlesource.com/platform/external/protobuf/+/master/java/README.txt) - it's protobufs but optimized for mobile development
                3. [FlatBuffers](https://google.github.io/flatbuffers/) - it is focused on performance
            - Don't serialize - The truth is that sometimes the most performance solution is to not use serialization at all. 
                - For example, if you are trying to store user preferences, you shouldn't be storing that as a serialized file, instead,  try using the [SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences.html) API. Which is basically a fast key-value store and perfect for this type of data.
                - Same goes for passing data between running processes, serialization is still the wrong answer here. Instead, use the [Parcelable](https://developer.android.com/reference/android/os/Parcelable.html) API, which gives you a slightly serialized format, but with a huge performance boost.
                - If you've got a lot of structured data that you plan on serializing... don't! The overhead of walking those serialized objects , not to mention the time it takes to load them, is horrible, compared to creating a local database with [SQLite](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html) API
    - Comments:
        - if you want to implement Parcelable quickly and easy visit this site: http://www.parcelabler.com and http://dallasgutauckis.com/2012/01/20/parcelabler-for-implementing-androids-parcelable-interface/ , it does the hard work.﻿
        - The problem I have with flat buffers is you have to compile your data schemas's beforehand. I wish there was a parcable type API that can save to Disk﻿
        - I'm trying to imagine how difficult it can be to debug with binary seralization﻿
- Episode 15 - Smaller Serialized Data

## Season 5

## Android N
- [Learn all about the Android N Developer Preview](http://goo.gl/44qIMd)
- [Check out the API Overview](http://goo.gl/xa204A)
- [And the Behavior Changes](http://goo.gl/Gsnlej)
- https://medium.com/google-developers/n-as-in-so-early-it-s-not-named-yet-f06bbde8c390#.r7ri4wmyw
- And learn more about how you can build your apps for:
    - Doze  
    - Multi-window  
    - Picture-in-picture  
    - Data Saver  
    - Newly-redesigned notifications (with Direct Reply and grouping)  
    - Direct Boot  
    - Scoped Directory Access 
    - Background optimizations and JobScheduler  
    - [Java 8 language features](https://developer.android.com/preview/j8-jack.html) that work back to Gingerbread (Android 2.3)  
